# TukoSoko Gateway

### dependency
```shell
$ nib
```
This is a docker compose wrapper

### running the app
```shell
$ nib build
$ nib up
```

### migrations
```shell
$ nib run api bin/rake db:migrate
```

### docker db
```shell
$ docker exec -it db psql -U badger -d {{db_name}}
```
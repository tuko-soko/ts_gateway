# frozen_string_literal: true

require 'rails_helper'

RSpec.describe 'Users', type: :request do
  describe 'GET /index' do
    it 'returns http success' do
      get '/users'
      expect(response).to have_http_status(200)
    end
  end

  describe 'POST /index' do
    it 'can create a user with valid attributes' do
      post '/users', params: {
        data: {
          type: 'users',
          attributes: {
            name: 'test',
            slug: 'testslug',
            email: 'test@mail.io',
            password: 'encryptedPassword!'
          }
        }
      }.to_json, headers: { 'Content-Type': 'application/vnd.api+json' }

      expect(response.status).to eq(201)
      expect(User.count).to equal(1)
    end
  end

  describe 'PUT /users/:id' do
    let!(:user) { User.create(name: 'name', slug: 'slug', email: 'mail@io.io', password: 'pass123') }
    it 'can update a user' do
      put "/users/#{user.id}", params: {
        data: {
          type: 'users',
          id: user.id,
          attributes: {
            name: 'nameSlug'
          }
        }
      }.to_json, headers: { 'Content-Type': 'application/vnd.api+json' }

      expect(response.status).to eq(200)
      expect(user.reload.name).to eq('nameSlug')
    end
  end
end

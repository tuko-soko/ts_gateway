# frozen_string_literal: true

FactoryBot.define do
  factory :user do
    name { Faker::Name.feminine_name }
    slug { Faker::Name.suffix }
    email { Faker::Internet.email }
    password { Faker::Internet.password }
  end
end

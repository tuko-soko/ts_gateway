# frozen_string_literal: true

class UserResource < JSONAPI::Resource
  attributes :name, :slug, :email, :password

  def fetchable_fields
    super - [:password]
  end
end

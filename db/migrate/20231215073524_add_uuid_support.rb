# frozen_string_literal: true

class AddUuidSupport < ActiveRecord::Migration[7.1]
  def change
    enable_extension 'uuid-ossp'
    enable_extension 'pgcrypto'
  end
end

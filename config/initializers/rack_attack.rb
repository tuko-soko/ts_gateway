# frozen_string_literal: true

Rack::Attack.throttle('requests by ip', limit: 5, period: 2, &:ip)

Rack::Attack.throttle('limit logins per email', limit: 5, period: 60) do |req|
  if req.path == '/oauth/token' && req.post?
    # Normalize the email, using the same logic as your authentication process, to
    # protect against rate limit bypasses.
    req.params['email'].to_s.downcase.gsub(/\s+/, '')
  end
end
